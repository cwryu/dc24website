from django.urls import include, re_path
from dc24.views import now_or_next


urlpatterns = [
    re_path(r'^now_or_next/(?P<venue_id>\d+)/$', now_or_next, name="now_or_next")
]
