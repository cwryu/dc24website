---
name: About DebConf
---
# About DebConf

DebConf is the annual conference for [Debian](http://debian.org) contributors
and users interested in [improving Debian](https://www.debian.org/intro/help).
[Previous Debian conferences](https://www.debconf.org) have featured speakers
and attendees from all around the world. The last DebConf, [DebConf23][], took
place in Kochi, India and was attended by **FIXME** participants from over
**FIXME** countries.

[DebConf23]: https://debconf23.debconf.org/

**DebConf24 is taking place in Busan, South Korea from July 28 to August 04,
2024.**

It is being preceded by DebCamp, from July 21 to July 27, 2023.

<form method="POST" action="https://lists.debian.org/cgi-bin/subscribe.pl">
  <fieldset>
    <legend>Register to the debconf-announce mailing list</legend>
    <label for="user_email">Your email address:</label>
    <input name="user_email" size="40" value="" type="Text">
    <input type="hidden" name="list" value="debconf-announce">
    <input name="action" value="Subscribe" type="Submit">
  </fieldset>
</form>

<br />

## Venue

FIXME

<br />
## Codes of Conduct and Community Team

Please see the [Code of Conduct](../coc/) page for more information.

We look forward to seeing you in Busan!
