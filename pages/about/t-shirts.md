---
name: T-Shirt Sizes
---

# T-Shirt sizes

<style type="text/css">
#main table {
  width: auto;
}
</style>
<img src="{% static "img/tshirt-size.jpg" %}" style="float: right; max-width: 100%"/>

## Straight cut

| Size           | Length (A) | Chest (B) | Length (A) | Chest (B) |
|----------------|------------|-----------|------------|-----------|
| S (Small)      | 26 in      | 19 in     | 66 cm      | 48 cm     |
| M (Medium)     | 27 in      | 20 in     | 69 cm      | 51 cm     |
| L (Large)      | 28 in      | 21 in     | 71 cm      | 53 cm     |
| XL(Xtra Large) | 29 in      | 22 in     | 74 cm      | 56 cm     |
| 2XL            | 30 in      | 23 in     | 76 cm      | 58 cm     |
| 3XL            | 31 in      | 24 in     | 79 cm      | 61 cm     |
| 4XL            | 32 in      | 25 in     | 81 cm      | 64 cm     |
| 5XL            | 33 in      | 26 in     | 84 cm      | 66 cm     |

## Fitted cut

| Size           | Length (A) | Chest (B) | Length (A) | Chest (B) |
|----------------|------------|-----------|------------|-----------|
| S (Small)      | 24 in      | 17 in     | 61 cm      | 43 cm     |
| M (Medium)     | 25 in      | 18 in     | 64 cm      | 46 cm     |
| L (Large)      | 26 in      | 19 in     | 66 cm      | 48 cm     |
| XL(Xtra Large) | 27 in      | 20 in     | 69 cm      | 51 cm     |
| 2XL            | 28 in      | 21 in     | 71 cm      | 52 cm     |
