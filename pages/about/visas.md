---
name: Visas
---

# Korean Visas and K-ETA

South Korea offers Visa waiver program to many number of countries. If you're visiting South Korea for short-term (up to 90 days) and eligible for visa waiver program, Normally you'll be required to apply for K-ETA. In case you need Visa to visit South Korea, You'll most likely need [C-3-1(Short-Term General) Visa](https://www.visa.go.kr/openPage.do?MENU_ID=1010201) which can be used to participate meetings and conferences.

To check if you need Visa to visit South Korea, Visit [Visa Navigator on visa.go.kr](https://www.visa.go.kr/openPage.do?MENU_ID=10101).

## K-ETA

Travellers who can visit South Korea without Visa are required to have a K-ETA (electronic travel authorization) issued in advance. Visitors in the following categories are not required to apply for K-ETA or are exempted from K-ETA:

- People holding a passport from the following countries who plan to visit South Korea by the end of 2024 - [see notice on k-eta.go.kr](https://www.k-eta.go.kr/portal/board/viewboarddetail.do?bbsSn=149899)
    - Australia, Austria, Belgium, Canada, Denmark, Finland, France, Germany, Hong Kong, Italy, Japan, Macao, Netherlands, New Zealand, Norway, Poland, Singapore, Spain, Sweden, Taiwan, UK, US (including Guam)
- Travellers aged 17 years old or younger, or 65 years old or older at the time of arrival - [See FAQ on k-eta.go.kr](https://www.k-eta.go.kr/portal/board/viewboardlist.do?tmpltNm=faq)
- Holders of a valid Diplomatic passport, official passport, APEC Business Travel Card or UN Passport
- Flight attendants and Seafarers
- Active service members of the United States Armed Forces according to the US-ROK Status of Forces Agreement (SOFA)
    - Note: Dependents of USFK members and civilian component, and contractors are also K-ETA exempted but need to obtain approval for K-ETA exemption in advance.

If you are required to apply for a K-ETA, [go to application form webpage](https://www.k-eta.go.kr/portal/apply/viewstep1.do) and start preparing your application. You may check your application result [here.](https://www.k-eta.go.kr/portal/apply/viewapplysearch.do)

- Your K-ETA must be issued before taking a flight or ferry to South Korea. K-ETA approval normally only takes a few hours, but we **strongly** recommended applying at least 72 hours in advance in case of delays.
- Be sure to double-check all information before submitting your form, since your personal (or identity) information such as names, passport numbers cannot be edited after submission. Even with incorrect personal information, your application might be approved but your K-ETA remains invalid. If that happens, you'll need to apply again with another fee payment. 
- Application guide can be found [here](https://www.k-eta.go.kr/portal/guide/viewetaapplication.do).
- Application fee is 10,000 KRW (approx. 8 USD). Fee is not refundable. More information on fee and payment can be found [here.](https://www.k-eta.go.kr/portal/guide/viewetafeeinformation.do)

## Preparing and Applying for C-3-1 Visa

To visit South Korea for DebConf24 with a visa, a C-3 visa (C-3-1 Short-Term General or C-3-9 Ordinary Tourist) should be sufficient. Among C-3 series of visa, we recommend applying for C-3-1 visa to make your purpose of visit clearer. 

If you need a C-3-1 visa and therefore an invitation from the conference sponsors, we recommend you register to attend and then let us know as early as possible. This is because visa applications usually take 2 weeks or more, and sometimes we need to send you document printouts physically via the postal service.

### What you (the attendee) will need to prepare

To obtain this kind of visa, you will need to prepare the following documents. The required documents vary by residency and passport authority, so these are general guidelines. Check the visa application documentation carefully for your particular circumstances.

- Your passport with more than 6 month of validity (Copy, original or both depending on diplomatic mission)
- Recently taken passport photo (1 or more depending on diplomatic mission)
- [Visa application form](https://www.visa.go.kr/downfile/VisaapplicationForm_EN.pdf) (Depending on diplomatic mission: printed then handwritten, or information filled in then printed, or [prepared with e-Form](https://www.visa.go.kr/openPage.do?MENU_ID=10204))
- Documents that shows your financial ability to travel South Korea.
    - If you own a business: Business profile, Business registration or both
    - If you're an employee: Certificate of employment and Salary slip
    - If you're a student: Certificate of enrollment
    - Recent bank statement and income tax return, personal (for employee) or for your business (for business owners)
        - Could be required or not and required period range vary depending on the diplomatic mission.
- Documents that show your purpose of visiting South Korea (may be required or not depending on the diplomatic mission)
    - Cover letter that includes your plan in South Korea, travel funding details and more.
    - Booking confirmation of accomodation and round trip flight or ferry ticket.

### What we (organizers) will provide to attendee for visa application and how to request

You will also need to receive following documents below either via email or via postal service from us:

- Invitation letter
- Letter of Guarantee (if the diplomatic mission requires this)
- Business registration
- Event brochure

We can provide you documents above once your registration is confirmed. To request, please contact [visa@debconf.org](mailto:visa@debconf.org) with details below. 

- Full name, as it appears on your passport
- Passport number
- Date of birth
- Place (country) of birth
- Nationality
- Photos of page(s) of your passport with personal information, personal photograph and passport number.
- Your travel itinerary to South Korea and where you plan to stay (with address).
- Information on your travel funding: Will it be funded by yourself, through bursary, your employer or other organization?

### Applying for Visa at South Korea Diplomatic Missions nearby

> **_NOTE:_**  This is still being written. Come and check-back later.
